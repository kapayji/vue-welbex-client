import { createStore } from "vuex";

const store = createStore({
  state: {
    user: null,
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
    delUser(state) {
      state.user = null;
    },
  },
  actions: {
    setUser({ commit }, user) {
      commit("setUser", user);
    },
    delUser({ commit }) {
      commit("delUser");
    },
  },
  getters: {
    getUser(state) {
      return state.user;
    },
  },
});
export default store;
