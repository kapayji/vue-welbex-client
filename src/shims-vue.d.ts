/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  $route: RouteLocationNormalizedLoaded
  const component: DefineComponent<{}, {}, any>
  export default component
}
