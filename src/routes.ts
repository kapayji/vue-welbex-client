import { createRouter, createWebHistory } from "vue-router";
import NotesList from "@/components/NotesList.vue";
import Login from "@/components/forms/Login.vue";
import Registration from "@/components/forms/Registration.vue";

const routerHistory = createWebHistory();

const router = createRouter({
  history: routerHistory,
  routes: [
    {
      path: "/",
      name: "home",
      component: NotesList,
    },
    {
      path: "/:user",
      name: "user",
      component: NotesList,
    },
    {
      path: "/login",
      name: "login",
      component: Login,
    },
    {
      path: "/registration",
      name: "registration",
      component: Registration,
    },
  ],
});
export default router;
