import { message } from "ant-design-vue";
export async function postRequestToRegOrLogin(body, routPath) {
  const response = await fetch(
    `${process.env.VUE_APP_API_URL}/auth/${routPath}`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    }
  );
  const data = await response.json();
  console.log(response.ok);
  if (!response.ok) {
    message.error(data.message);
    return;
  }
  message.success("Успешно");
  return data;
}
export async function getRequestOnRefresh(param, token) {
  const response = await fetch(`${process.env.VUE_APP_API_URL}/user/${param}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  });
  const data = await response.json();
  if (!response.ok) {
    message.error("Необходимо перезайти");
    return false;
  }
  return data;
}
